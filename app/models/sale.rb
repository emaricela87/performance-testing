class Sale < ActiveRecord::Base

  paginates_per 50

  belongs_to :customer


  def amount_with_tax
    self.amount + self.tax
  end
end
